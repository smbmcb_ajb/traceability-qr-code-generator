<?php

include '../1Connection.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');

$currentDate = $currentYear . "-" . $currentMonth . "-" . $currentDay;

$assy_line = $_POST['assy_line'] ?? '';
$invoice = $_POST['invoice'] ?? '';
$date_receive = $_POST['date_receive'] ?? '';
$supplier = $_POST['supplier'] ?? '';
$goods_code = $_POST['goods_code'] ?? '';
$materials = $_POST['materials'] ?? '';
$item_code = $_POST['item_code'] ?? '';
$part_number = $_POST['part_number'] ?? '';
$part_name = $_POST['part_name'] ?? '';
$quantity = $_POST['quantity'] ?? '';
$p_slip = $_POST['p_slip'] ?? '';
$s_slip = $_POST['s_slip'] ?? '';
$archieve = 0;

// echo $assy_line . " " . $invoice . " " . $date_receive . " " . $supplier . " " . $goods_code . " " . $materials . " " . $item_code . " " . $part_number . " " . $part_name . " " . $quantity;

$query = sqlsrv_query( $conn, "SELECT * FROM [Receive] 
WHERE ASY_LINE = '$assy_line'
AND INVOICE ='$invoice'
AND DATE_RECEIVE = '$date_receive'
AND SUPPLIER = '$supplier'
AND GOODS_CODE = '$goods_code'
AND MATERIALS_TYPE = '$materials'
AND ITEM_CODE = '$item_code'
AND PART_NUMBER = '$part_number'
AND PART_NAME = '$part_name'
AND QTY = '$quantity'
AND ARCHIVE = '$archieve' 
AND QTY_S = '$quantity' ", array());

if ($query !== NULL) {  

    $rows = sqlsrv_has_rows( $query );

    if ($rows === true) {
        echo 'Our report already includes the data from this row.';
    }

    else{

        if ($quantity === ''){
            echo "ERROR";
        }

        else{

            $tsql = "INSERT INTO [Receive] 
            (GOODS_CODE, ASY_LINE, SUPPLIER, MATERIALS_TYPE, ITEM_CODE, PART_NUMBER, PART_NAME, QTY, DATE_RECEIVE, INVOICE, ARCHIVE, P_SLIP, QTY_S, S_SLIP, STATUS)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
            $params1 = array(
                $goods_code, 
                $assy_line, 
                $supplier, 
                $materials, 
                $item_code, 
                $part_number, 
                $part_name, 
                $quantity, 
                $date_receive,
                $invoice,
                $archieve,
                $p_slip,
                $quantity,
                $s_slip,
                'RAW');
        
            $stmt1 = sqlsrv_query( $conn, $tsql, $params1);
                
            if( $stmt1 ){

                echo 'Upload was successful.';

                // QUERY PARA SA PAG INSERT AT UPDATE NG TOTAL STOCK NG MATERIALS
                // $query2 = sqlsrv_query( $conn, "SELECT * FROM [Total_Stock] 
                // WHERE GOODS_CODE = '$goods_code'
                // AND ITEM_CODE ='$item_code'", array());

                // if ($query2 !== NULL) {  

                //     $rows = sqlsrv_has_rows( $query2 ); 
                    
                //     if ($rows === true) {

                //         // QUERY PARA KUNIN YUNG TOTAL STOCKS NG EXISTING NA MATERIAL
                //         $query1 = "SELECT * FROM [dbo].[Total_Stock] 
                //         WHERE GOODS_CODE = '$goods_code'
                //         AND ITEM_CODE ='$item_code'";
                //         $result = sqlsrv_query($conn, $query1);

                //         while($row=sqlsrv_fetch_array($result)){

                //             $OLD_TOTAL_STOCK = $row['TOTAL_STOCK'];
                //             $ADD_COUNT = $row['ADD_COUNT'];

                //         }
                //         $NEW_TOTAL_STOCK = $OLD_TOTAL_STOCK + $quantity;
                //         $NEW_ADD_COUNT = $ADD_COUNT + 1;
                        
                //         $tsql = "UPDATE [Total_Stock] 
                //         SET TOTAL_STOCK = '$NEW_TOTAL_STOCK',
                //         ADD_COUNT = '$NEW_ADD_COUNT'
                //         WHERE GOODS_CODE = '$goods_code'
                //         AND ITEM_CODE ='$item_code'";
                            
                //         $stmt2 = sqlsrv_query( $conn, $tsql);
                                    
                //         if( $stmt2 ){

                //             date_default_timezone_set("Asia/Hong_Kong");
                //             $current_date_with_time =  date("Y-m-d H:i:s");

                //             //QUERY PARA MAKAPAG LAGAY NG DATA SA STOCK CARD REPORT
                //             $tsqlStockCard = "INSERT INTO [transaction_record_tbl] 
                //             (TRANSACTION_DATE, GOODS_CODE, ITEM_CODE, QTY_ISSUED, TOTAL_STOCK, PART_NUMBER, PART_NAME, QTY_RECEIVED, INVOICE_KIT)
                //             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                                    
                //             $params1StockCard = array(
                //                 $current_date_with_time,
                //                 $goods_code, 
                //                 $item_code,
                //                 0,
                //                 $NEW_TOTAL_STOCK,
                //                 $part_number, 
                //                 $part_name, 
                //                 $quantity, 
                //                 $invoice);

                //             $stmt1StockCard = sqlsrv_query( $connStock, $tsqlStockCard, $params1StockCard);

                //             if( $stmt1StockCard ){

                //             }
                                                                        
                //             else
                //             {
                //                 echo 'Error: The system is unable to update the quantity of stocks; thus, please contact the developer as soon as possible.';
                //                 die( print_r( sqlsrv_errors(), true));
                //             }

                //         }
                                                                    
                //         else
                //         {
                //             echo 'Error: The system is unable to update the quantity of stocks; thus, please contact the developer as soon as possible.';
                //             die( print_r( sqlsrv_errors(), true));
                //         }

                //     }

                //     else {
                
                //         // if ($quantity === ''){
                //         //     echo "Error: We can not process this transaction because the quantity is empty.";
                //         // }
                //         // else{
                
                            
                //         // }  
                //         $tsql = "INSERT INTO [Total_Stock] 
                //         (GOODS_CODE, ITEM_CODE, MATERIALS, TOTAL_STOCK, PART_NUMBER, PART_NAME, ADD_COUNT)
                //         VALUES (?, ?, ?, ?, ?, ?, ?)";
                    
                //         $params1 = array(
                //             $goods_code,
                //             $item_code,
                //             $materials,
                //             $quantity, 
                //             $part_number, 
                //             $part_name,
                //             1);
                    
                //         $stmt1 = sqlsrv_query( $conn, $tsql, $params1);
                            
                //         if( $stmt1 ){

                //             echo "This new item has been successfully added to total stock.";

                //             date_default_timezone_set("Asia/Hong_Kong");
                //             $current_date_with_time =  date("Y-m-d H:i:s");

                //             // QUERY PARA MAKAPAG LAGAY NG DATA SA STOCK CARD REPORT
                //             $tsqlStockCard = "INSERT INTO [transaction_record_tbl] 
                //             (TRANSACTION_DATE, GOODS_CODE, ITEM_CODE, QTY_ISSUED, TOTAL_STOCK, PART_NUMBER, PART_NAME, QTY_RECEIVED, INVOICE_KIT)
                //             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                                    
                //             $params1StockCard = array(
                //                 $current_date_with_time,
                //                 $goods_code, 
                //                 $item_code,
                //                 0,
                //                 $quantity,
                //                 $part_number, 
                //                 $part_name, 
                //                 $quantity, 
                //                 $invoice);

                //             $stmt1StockCard = sqlsrv_query( $connStock, $tsqlStockCard, $params1StockCard);

                //             if( $stmt1StockCard ){

                //             }
                                                                        
                //             else
                //             {
                //                 echo 'Error: The system is unable to update the quantity of stocks; thus, please contact the developer as soon as possible.';
                //                 die( print_r( sqlsrv_errors(), true));
                //             }

                //         }
                                                            
                //         else
                //         {
                //             echo "The attempt to add this new item to the total inventory was unsuccessful.";
                //             die( print_r( sqlsrv_errors(), true));
                //         }
                //     }
                // }
            }
                                                
            else
            {
                echo 'The upload failed.';
                die( print_r( sqlsrv_errors(), true));
            }
        }
    }
}




?>