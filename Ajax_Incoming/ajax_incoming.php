<?php

include '../1Connection.php';

?>

<div class="Container pad_30 mt_30">

    <div class="container vertical_center margin_bottom">
        <label class="date_label" for="from">Start: </label>
        <input class="mr_20 date_picker" type="date" id="from" name="from">

        <label class="date_label" for="to">End: </label>
        <input class="mr_20 date_picker" type="date" id="to" name="to">
    </div>

    <div class="container vertical_center">
        <button class="global_button mr_20" id="expected_all">All</button>
        <button class="completed mr_20" id="expected_comp">Completed</button>
        <button class="remain mr_20" id="expected_rem">Remaining</button>
    </div>

    <div id="incoming_content"></div>
</div>

<script>

    var incoming_table = "/Ajax_Incoming/ajax_inc_table.php";

    $(document).ready(function(){
        $('#incoming_content').load(incoming_table);
        document.getElementById("to").disabled = true;
    });

    var from = '';
    var to = '';
    var picked = '';

    $('#from').on('change', function() {
        from = this.value;

        if(from == ''){
            $('#to').val('');
            document.getElementById("to").disabled = true;
            $('#incoming_content').load(incoming_table);
        }
        else if(from > to && to != '') {
            $('#to').val('');
            alert("Error: Invalid because the start date exceeds the end date.");   
            this.value = ''; 

            $.ajax({
                type: "POST",
                url: '../Ajax_Incoming/ajax_inc_table.php',
                data: {

                },
                cache: false,
                success: function(data) {
                    $('#incoming_content').html(data);
                },
                error: function(xhr, status, error) {
                    console.error(xhr);
                }

            });

        }
        else {
            document.getElementById("to").disabled = false;
            $.ajax({
                type: "POST",
                url: '../Ajax_Incoming/ajax_inc_table.php',
                data: {
                    from: from,
                    to: to

                },
                cache: false,
                success: function(data) {
                    $('#incoming_content').html(data);
                },
                error: function(xhr, status, error) {
                    console.error(xhr);
                }

            });
        }
    });

    $('#to').on('change', function() {
        to = this.value;

        if(to == '') {
            $.ajax({
                type: "POST",
                url: '../Ajax_Incoming/ajax_inc_table.php',
                data: {
                    from: from,
                    to: to

                },
                cache: false,
                success: function(data) {
                    $('#incoming_content').html(data);
                },
                error: function(xhr, status, error) {
                    console.error(xhr);
                }

            });
        }
        else if(from > to) {
            alert("Error: Invalid because the start date exceeds the end date.");   
            this.value = ''; 
        }
        else {
            $.ajax({
                type: "POST",
                url: '../Ajax_Incoming/ajax_inc_table.php',
                data: {
                    from: from,
                    to: to

                },
                cache: false,
                success: function(data) {
                    $('#incoming_content').html(data);
                },
                error: function(xhr, status, error) {
                    console.error(xhr);
                }

            });
        }
    });

    $("#expected_all").click(function() {

        picked = 'all';

        $.ajax({
            type: "POST",
            url: '../Ajax_Incoming/ajax_inc_table.php',
            data: {
                from: from,
                to: to,
                picked: picked

            },
            cache: false,
            success: function(data) {
                $('#incoming_content').html(data);
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }

        });

    });

    $("#expected_comp").click(function() {

        picked = 'comp';

        $.ajax({
            type: "POST",
            url: '../Ajax_Incoming/ajax_inc_table.php',
            data: {
                from: from,
                to: to,
                picked: picked

            },
            cache: false,
            success: function(data) {
                $('#incoming_content').html(data);
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }

        });

    });

    $("#expected_rem").click(function() {

        picked = 'rem';

        $.ajax({
            type: "POST",
            url: '../Ajax_Incoming/ajax_inc_table.php',
            data: {
                from: from,
                to: to,
                picked: picked

            },
            cache: false,
            success: function(data) {
                $('#incoming_content').html(data);
            },
            error: function(xhr, status, error) {
                console.error(xhr);
            }

        });

    });

</script>