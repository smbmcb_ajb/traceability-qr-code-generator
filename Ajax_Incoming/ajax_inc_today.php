<?php

include '../1Connection.php';

$currentYear = date('Y');
$currentMonth = date('m');
$currentDay = date('d');

if ($currentMonth == "01" || 
    $currentMonth == "03" || 
    $currentMonth == "05" || 
    $currentMonth == "07" || 
    $currentMonth == "08" || 
    $currentMonth == "10" || 
    $currentMonth == "12") {

    $EndDayOfTheMonth = "31";

}

elseif( $currentMonth == "04" || 
        $currentMonth == "06" || 
        $currentMonth == "09" || 
        $currentMonth == "11") {

        $EndDayOfTheMonth = "30";

}

else {

    $EndDayOfTheMonth = "28";

}

$query = "SELECT * FROM [dbo].[POLISTOrigdownloadToss]
WHERE [Req# dlv] = '$currentYear-$currentMonth-$currentDay'";

$result = sqlsrv_query($conn_ms21_dup, $query);

$counter = '0';

while($rows=sqlsrv_fetch_array($result)){
    $goods_code[$counter] = $rows['Goods code'];
    $deli_date[$counter] = $rows['Req# dlv']->format('Y-m-d');
    $deli_date_m[$counter] = $rows['Req# dlv']->format('m');
    $toss_po[$counter] = $rows['Slip No'];
    $po_qty[$counter] = $rows['P/O qty'];
    $po_deli[$counter] = $rows['Fin of Pur qty'];
    $po_bal_qty[$counter] = $rows['P/O balance qty'];
    $ms21_po[$counter] = $rows['E/U P/O No'];
    $po_date[$counter] = $rows['P/O date'];
    // $supplier[$counter] = $rows['Cstmr name'];
    $supplier[$counter] = $rows['Vender name'];

    $counter = $counter + 1;
}

?>

<div class="center"><p class="info_area_text">Scheduled Materials Delivery</p></div>

<table id="incoming_table_today" class="text_center fixed">

    <thead>
        <tr>
            <th>GOODS CODE</th>
            <th>SUPPLIER</th>
            <th>Target Delivery Date</th>
            <th>TOSS PO</th>
            <th>PO QTY</th>
            <th>DELIVERED QTY</th>
            <th>PO BALANCE QTY</th>
            <th>MS21 PO</th>
        </tr>
    </thead>

    <tbody>
            
        <?php
            for ($x = 0; $x <= $counter; $x++) {

                $color = "#fff";

                if ($deli_date_m[$x] == '01'){ $bg = "#0000FF"; }
                elseif ($deli_date_m[$x] == '02'){ $bg = "#8F00FF"; }
                elseif ($deli_date_m[$x] == '03'){ $bg = "#F47F39"; }
                elseif ($deli_date_m[$x] == '04'){ $bg = "#A2B2AC"; $color = "#000"; }
                elseif ($deli_date_m[$x] == '05'){ $bg = "#4C9A2A"; }
                elseif ($deli_date_m[$x] == '06'){ $bg = "#0D0C12"; }
                elseif ($deli_date_m[$x] == '07'){ $bg = "#FFC0CB"; $color = "#000"; }
                elseif ($deli_date_m[$x] == '08'){ $bg = "#964B00"; }
                elseif ($deli_date_m[$x] == '09'){ $bg = "#FED758"; $color = "#000"; }
                elseif ($deli_date_m[$x] == '10'){ $bg = "#7AD7F0"; $color = "#000"; }
                elseif ($deli_date_m[$x] == '11'){ $bg = "#FFFDFA"; $color = "#000"; }
                else{$bg = "#880808";}

                if($goods_code[$x] != ''){
                    echo "<tr style='color:".$color."; background:".$bg.";'>
                            <td>" . $goods_code[$x] . "</td>
                            <td>" . $supplier[$x] . "</td>
                            <td>" . $deli_date[$x] . "</td>
                            <td>" . $toss_po[$x] . "</td>
                            <td>" . number_format($po_qty[$x]) . "</td>
                            <td>" . number_format($po_deli[$x]) . "</td>
                            <td>" . number_format($po_bal_qty[$x]) . "</td>
                            <td>" . $ms21_po[$x] . "</td>
                        </tr>";

                }
            }
        ?>

    </tbody>

</table>


<!-- SCRIPT FOR DATATABLE -->
<script>
    $('#incoming_table_today').DataTable({ 
        // "ordering": false,
        order: [[2, 'asc']],
        // "lengthChange": false, // disable the length 10/25/50/100
        "pagingType": "simple_numbers", //paging type numbers/simple/simple_numbers/full/full_numbers/first_last_numbers
        // "pageLength": 12,
        "searching": false
    });
</script>