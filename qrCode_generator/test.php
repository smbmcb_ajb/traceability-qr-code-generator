<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>jQuery UI Datepicker - Default functionality</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.9/sweetalert2.min.js"></script>
  
</head>
<body>
 
<p>Date: <button id="button">click</button></p>
 
<script>
  $('#button').click(() => {
  swal({
    title: 'Date picker',
    html: '<input id="datepickerss"></input>',
    onOpen: function() {
    	$('#datepickerss').datepicker();
    },
    preConfirm: function() {
      return Promise.resolve($('#datepickerss').datepicker('getDate'));
    }
  }).then(function(result) {
    swal({
      type: 'success',
      html: 'You entered: <strong>' + result + '</strong>'
    });
  });
});
  </script>
  
</body>
</html>